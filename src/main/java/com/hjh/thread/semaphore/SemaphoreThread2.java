package com.hjh.thread.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 线程信号量Semaphore的运用
 * 
 * @author XIEHEJUN
 * 
 */
public class SemaphoreThread2 {

    public static void main(String[] args) {
        Semaphore semaphore =     new Semaphore(5,true);
        System.out.println("=======>"+semaphore.availablePermits());
        semaphore.release();
        System.out.println("=======>"+semaphore.availablePermits());
        semaphore.release();
        System.out.println("=======>"+semaphore.availablePermits());
        semaphore.release();
        System.out.println("=======>"+semaphore.availablePermits());
        System.out.println("=======>"+semaphore.availablePermits());
    }
}