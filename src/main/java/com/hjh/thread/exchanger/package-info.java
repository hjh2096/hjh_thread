package com.hjh.thread.exchanger;

/*
1、exchanger是java提供的一个并发工具，实现两个线程之间的数据交换；

2、exchanger的要点
此类提供对外的操作是同步的；
用于成对出现的线程之间交换数据；
可以视作双向的同步队列；
可应用于基因算法、流水线设计等场景。


3、当一个线程到达exchange调用点时，如果它的伙伴线程此前已经调用了此方法，
  那么它的伙伴会被调度唤醒并与之进行对象交换，然后各自返回。
  如果它的伙伴还没到达交换点，那么当前线程将会被挂起，直至伙伴线程到达——完成交换正常返回；
  或者当前线程被中断——抛出中断异常；又或者是等候超时——抛出超时异常。



 */

