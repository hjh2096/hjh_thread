package com.hjh.thread.bean.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;


/**
 * <p>
 *
 * </p>
 *
 * @author z77z
 * @since 2017-01-22
 */
@TableName("web_base")
public class WebBase extends Model<WebBase> {

    private static final long serialVersionUID = 1L;

    /**
     * 每张图片的地址
     */
    @TableId
    private Long id;

    @TableField("year")
    private String year;

    @TableField("num")
    private Integer num;

    @TableField("animal")
    private String animal;

    @TableField("color")
    private String color;

    @TableField("iBig")
    private Integer iBig;

    @TableField("kill_animal")
    private String killAnimal;

    @TableField("kill_num")
    private Integer killNum;

    @TableField("ext")
    private String ext;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getiBig() {
        return iBig;
    }

    public void setiBig(Integer iBig) {
        this.iBig = iBig;
    }

    public String getKillAnimal() {
        return killAnimal;
    }

    public void setKillAnimal(String killAnimal) {
        this.killAnimal = killAnimal;
    }

    public Integer getKillNum() {
        return killNum;
    }

    public void setKillNum(Integer killNum) {
        this.killNum = killNum;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}