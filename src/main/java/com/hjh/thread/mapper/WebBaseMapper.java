package com.hjh.thread.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hjh.thread.bean.model.WebBase;

public interface WebBaseMapper extends BaseMapper<WebBase> {

    Integer selectCount();
}
