package com.hjh.thread.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjh.thread.bean.model.WebBase;
import com.hjh.thread.mapper.WebBaseMapper;
import org.springframework.stereotype.Service;

@Service
public class WebBaseService extends ServiceImpl<WebBaseMapper,WebBase> {

    public String test(){
        return "test1";
    }
    public String getCount(){
        Integer count = this.baseMapper.selectCount();
        return "test1,count:"+count;
    }
}
