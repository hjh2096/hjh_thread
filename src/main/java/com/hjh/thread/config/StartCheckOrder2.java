package com.hjh.thread.config;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.PriorityOrdered;

@Configuration
public class StartCheckOrder2 implements ApplicationListener<ContextRefreshedEvent> , PriorityOrdered {


    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("========>>>>>>>>>test");

        System.out.println("我的父容器为2：" + contextRefreshedEvent.getApplicationContext().getParent());
        System.out.println("初始化时我被调用了2。");
    }


    @Override
    public int getOrder() {
        System.out.println("=============>>demo getOrder2");
        return 2;
    }


}
