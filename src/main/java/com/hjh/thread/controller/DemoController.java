package com.hjh.thread.controller;

import com.hjh.thread.bean.model.WebBase;
import com.hjh.thread.service.WebBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private WebBaseService webBaseService;

    @RequestMapping("/test1")
    public String test1(){
        return "hello springboot!";
    }

    @RequestMapping("/test2")
    public String test2(){
        return "hello springboot!"+webBaseService.test();
    }

    @RequestMapping("/test3")
    public String test3(){
        return "hello springboot!"+webBaseService.getCount();
    }
}
